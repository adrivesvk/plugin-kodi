import xbmcgui
import xbmcplugin

from resources.lib.compatibility import ListItem
from resources.lib.const import LANG, ICON, ROUTE, MEDIA_TYPE, TRAKT_LIST, MEDIA_SERVICE, COMMAND
from resources.lib.gui.renderers.media_info_renderer import MediaInfoRenderer
from resources.lib.gui.text_renderer import TextRenderer
from resources.lib.routing.router import router
from resources.lib.storage.sqlite import DB
from resources.lib.utils.kodiutils import get_string, get_icon, ADDON, clear_none_values


class DirectoryItem(object):
    TITLE = ''
    DIRECTORY = True
    ICON = ''
    URL = ''

    def __init__(self, title='', url='', icon='', context_menu=None, key=None, description='', media_type=None,
                 focus=False):
        if context_menu is None:
            context_menu = []
        self.title = title or self.TITLE
        self.url = url or self.URL
        self._icon = icon or self.ICON
        self.context_menu = context_menu
        self.key = key
        self.description = description
        self.media_type = media_type
        self.focus = focus

    def __call__(self, handle, *args, **kwargs):
        url, li, is_folder = self.build()
        xbmcplugin.addDirectoryItem(handle, url, li, isFolder=is_folder)

    def build(self):
        item = xbmcgui.ListItem(
            label=self.title,
            path=self.url,
        )
        item.setArt({
            'icon': self._icon,
        })
        item.setContentLookup(False)
        item.addContextMenuItems([x for x in self.context_menu if x is not None])
        item.setInfo('video', {'plot': self.description})
        return self.url, item, self.DIRECTORY,


class CommandItem(DirectoryItem):
    DIRECTORY = False
    ICON = get_icon(ICON.CALENDAR_PICKER)


class DatePickerItem(CommandItem):
    DIRECTORY = False
    ICON = get_icon(ICON.CALENDAR_PICKER)


class FilterItem(CommandItem):
    DIRECTORY = False
    ICON = get_icon(ICON.FILTER)


class DownloadQueueItem(DirectoryItem):
    ICON = get_icon(ICON.DOWNLOAD)
    TITLE = get_string(LANG.DOWNLOAD_QUEUE)


class WatchHistoryItem(DirectoryItem):
    ICON = get_icon(ICON.HISTORY)
    TITLE = get_string(LANG.WATCH_HISTORY)


class SearchItem(DirectoryItem):
    DIRECTORY = False
    ICON = get_icon(ICON.SEARCH)
    TITLE = get_string(LANG.SEARCH)
    URL = router.get_url(COMMAND.SEARCH)


class SearchHistoryItem(DirectoryItem):
    ICON = get_icon(ICON.SEARCH_HISTORY_2)
    TITLE = get_string(LANG.SEARCH_HISTORY)


class TvProgramItem(DirectoryItem):
    ICON = get_icon(ICON.TV_PROGRAM)
    TITLE = get_string(LANG.TV_PROGRAM)


class MoviesItem(DirectoryItem):
    ICON = get_icon(ICON.MOVIES)
    TITLE = get_string(LANG.MOVIES)


class TvShowsItem(DirectoryItem):
    ICON = get_icon(ICON.TVSHOWS)
    TITLE = get_string(LANG.TV_SHOWS)


class MainMenuFolderItem(DirectoryItem):
    ICON = get_icon(ICON.HOME)
    TITLE = get_string(30202)


class SeasonItem(DirectoryItem):
    ICON = 'DefaultTVShowTitle.png'


class SettingsItem(DirectoryItem):
    ICON = get_icon(ICON.SETTINGS)
    TITLE = get_string(30208)
    DIRECTORY = False


class TraktItem(DirectoryItem):
    ICON = get_icon(ICON.TRAKT)
    TITLE = get_string(LANG.TRAKT_MY_LISTS)
    URL = router.get_url(ROUTE.TRAKT_LISTS, user='me')


is_playable = {
    False: 'false',
    True: 'true',
}


class MediaItem(object):
    DIRECTORY = False

    def __init__(self, title, playable=True, url=None, art=None, cast=None, info_labels=None, stream_info=None,
                 services=None, context_menu=None, lite_mode=None, focus=False):
        if services is None:
            services = {}
        if context_menu is None:
            context_menu = []
        self._title = title
        self._url = url
        self._art = art
        self._cast = cast
        self._info_labels = info_labels
        self._stream_info = stream_info
        self._services = services
        self._cast = cast
        self._playable = playable
        self._context_menu = context_menu
        self.lite_mode = lite_mode
        self.focus = focus

    def __call__(self, handle):
        url, li, is_folder = self.build()
        xbmcplugin.addDirectoryItem(handle, url, li, isFolder=is_folder)

    @property
    def title(self):
        return self._title

    @property
    def media_type(self):
        return self._info_labels.get('mediatype')

    @property
    def sort_title(self):
        return self._info_labels.get('sorttitle')

    def delete_useless_labels(self):
        useless = ['parent_titles', 'labels_eng', 'lang']
        for key in useless:
            self._info_labels.pop(key, None)

    @property
    def play_count(self):
        record = DB.TRAKT_LIST_ITEMS.get_from_list(TRAKT_LIST.WATCHED, self.media_type,
                                                   self._services.get(MEDIA_SERVICE.TRAKT))
        return record[3] if record else None

    def build(self):
        """Creates the ListItem together with metadata."""
        item = ListItem(
            label=self._title,
            path=self._url,
        )
        self._info_labels.update({'title': self._title})

        if self.media_type == MEDIA_TYPE.TV_SHOW:
            self._info_labels.update({'tvshowtitle': self._title})
        elif self.media_type == MEDIA_TYPE.SEASON or self.media_type == MEDIA_TYPE.EPISODE:
            if not self._info_labels.get('season'): self._info_labels.update({'season': 1})       # FIX because DB is dependent on CSFD. CSFD sometimes ignores Season 1 on series. TODO: Delete after version 2.0
            self._info_labels.update(
                {'tvshowtitle': MediaInfoRenderer.get_root_title(self._info_labels['labels_eng']) or
                                MediaInfoRenderer.get_root_title(self._info_labels)})

        self.delete_useless_labels()

        clear_none_values(self._info_labels)
        clear_none_values(self._art)

        item.setArt(self._art)
        item.setInfo('video', self._info_labels)

        if not self.lite_mode:
            clear_none_values(self._stream_info)
            if self._stream_info:
                for key, value in self._stream_info.items():
                    item.addStreamInfo(key, value)
            if self._services:
                ids = {}
                for service in [MEDIA_SERVICE.TVDB, MEDIA_SERVICE.IMDB, MEDIA_SERVICE.TMDB]:
                    s_id = self._services.get(service)
                    if s_id:
                        ids.update({service: str(s_id)})
                item.setUniqueIDs(ids)
            item.setCast(self._cast)
        item.setProperty('IsPlayable', is_playable[self._playable])
        item.addContextMenuItems([x for x in self._context_menu if x is not None], True)
        return self._url, item, self.DIRECTORY,


class ParentItem(MediaItem):
    DIRECTORY = True

    def __init__(self, *args, **kwargs):
        super(ParentItem, self).__init__(*args, **kwargs)


class InfoDialogType:
    INFO = 'INFO'
    WARNING = 'WARNING'
    ERROR = 'ERROR'


class InfoDialog:
    def __init__(self, message, heading=ADDON.getAddonInfo('name'), icon='', time=3000, sound=False):
        self._heading = heading
        self._message = message
        self._icon = InfoDialog._get_icon(icon)
        self._time = time
        self._sound = sound

    @staticmethod
    def _get_icon(icon):
        if icon == '':
            return ADDON.getAddonInfo('icon')
        elif icon == InfoDialogType.INFO:
            return xbmcgui.NOTIFICATION_INFO
        elif icon == InfoDialogType.WARNING:
            return xbmcgui.NOTIFICATION_WARNING
        elif icon == InfoDialogType.ERROR:
            return xbmcgui.NOTIFICATION_ERROR

    def notify(self):
        xbmcgui.Dialog().notification(self._heading, self._message, self._icon, self._time, sound=self._sound)
