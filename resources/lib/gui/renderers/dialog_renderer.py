import datetime
import time

import xbmc
import xbmcgui
from xbmcgui import Dialog, DialogProgress

from resources.lib.compatibility import select, is_compact_stream_picker
from resources.lib.const import STRINGS, SETTINGS, LANG, lang_code_gui, audio_channels, stream_quality_map, COLOR
from resources.lib.gui import InfoDialog
from resources.lib.gui.renderers.icon_renderer import IconRenderer
from resources.lib.storage.settings import settings
from resources.lib.subtitles import Subtitles
from resources.lib.utils.kodiutils import show_input, get_string, convert_size, make_table, \
    append_list_items_to_nested_list_items, convert_bitrate, colorize
from resources.lib.utils.streams import resolution_to_quality


class DialogRenderer:

    @staticmethod
    def search():
        search_value = show_input(get_string(30207))
        if search_value:
            return search_value

    @staticmethod
    def get_subtitles(subtitles):
        languages = settings.get_plugin_languages()
        for sub in subtitles:
            for lang in languages:
                sub_lang = sub.get('language')
                if sub_lang == lang:
                    return sub_lang

    @staticmethod
    def choose_video_stream(streams):
        stream_labels = []
        streams.sort(key=lambda d: d.get('size', 0), reverse=settings[SETTINGS.FILE_SIZE_SORT])
        audio_info_list = []
        plugin_languages = settings.get_plugin_languages()
        is_compact = is_compact_stream_picker()
        for stream in streams:
            # Fix audio string that begins with the comma.
            audio_info = []
            for audio in stream.get('audio'):
                lang_code_l = audio.get('language', '').lower()
                lang_code = lang_code_gui.get(lang_code_l) or ''
                channels = audio.get('channels')
                channels = audio_channels.get(channels, channels)
                if lang_code:
                    lang_code = colorize(COLOR.LIGHTSKYBLUE, lang_code) if lang_code_l in plugin_languages else lang_code
                    info = STRINGS.AUDIO_INFO.format(audio.get('codec'),
                                                     format(channels, '.1f'),
                                                     lang_code)

                else:
                    info = STRINGS.AUDIO_INFO_NO_LANG.format(audio.get('codec'),
                                                             format(channels, '.1f'))
                audio_info.append(info)

            subtitles = DialogRenderer.get_subtitles(stream.get('subtitles')) or None
            if subtitles:
                lang_code = lang_code_gui.get(subtitles)
                lang_code = colorize(COLOR.LIGHTSKYBLUE, lang_code) if subtitles in plugin_languages else lang_code
                subtitles = STRINGS.PAIR.format(get_string(LANG.SUB),
                                                lang_code) + STRINGS.TABLE_SPACES
            else:
                subtitles = STRINGS.SPACE * 13

            audio_info_list.append(subtitles + STRINGS.SPACE.join(audio_info))
            video = stream.get('video')
            bit_rate = ''
            duration = ''
            size = ''
            quality = ''
            hdr = ''
            _3d = ''
            quality_raw = None

            if len(video) > 0:
                video = video[0]
                quality_raw = resolution_to_quality(video)
                quality = STRINGS.STREAM_TITLE_BRACKETS.format(quality_raw)
            else:
                video = {}

            if settings[SETTINGS.SHOW_HDR] and video.get('hdr'):
                hdr = STRINGS.STREAM_TITLE_BRACKETS.format(get_string(LANG.HDR))

            if stream.get('size'):
                size = STRINGS.BOLD.format(convert_size(stream.get('size')))

            if video.get('duration') and settings[SETTINGS.SHOW_DURATION]:
                duration = STRINGS.BOLD.format(str(datetime.timedelta(seconds=int(video.get('duration')))))

            if video.get('duration') and stream.get('size') and settings[SETTINGS.SHOW_BITRATE]:
                bit_rate = stream.get('size') / video.get('duration') * 8
                bit_rate = STRINGS.STREAM_BITRATE_BRACKETS.format(convert_bitrate(bit_rate))

            if video.get('3d'):
                _3d = STRINGS.STREAM_TITLE_BRACKETS.format('3D')

            codec = STRINGS.STREAM_TITLE_BRACKETS.format(video.get('codec')) if settings[
                SETTINGS.SHOW_CODEC] else ''
            if not is_compact:
                title_parts = [size, bit_rate, duration, codec, _3d]
            else:
                title_parts = [quality, STRINGS.SPACE.join([hdr, _3d, codec]), size, bit_rate, duration]
            stream_labels.append({
                'title_parts': title_parts,
                'quality': quality_raw,
                'label2': [],
                'hdr': hdr,
                '3d': _3d
            })

        table = make_table([i['title_parts'] for i in stream_labels])
        labels2 = make_table([i['label2'] for i in stream_labels])
        labels2 = append_list_items_to_nested_list_items(labels2, audio_info_list)

        list_items = []
        for i, item in enumerate(table):
            stream_label = stream_labels[i]
            quality = stream_label['quality']
            label2 = labels2[i]
            label = STRINGS.TABLE_SPACES.join(item)
            label2 = STRINGS.TABLE_SPACES.join([s for s in label2 if s])
            if is_compact:
                label = label + STRINGS.TABLE_SPACES + label2

            inner_item = xbmcgui.ListItem(
                label=label,
                label2=label2,
            )
            if quality:
                show_hdr = stream_label['hdr'] and settings[SETTINGS.SHOW_HDR]
                inner_item.setArt({
                    'icon': IconRenderer.quality(stream_quality_map.get(quality, 'invalid-url'), show_hdr, stream_label['3d']),
                })
            list_items.append(inner_item)

        ret = DialogRenderer.select(get_string(LANG.CHOOSE_STREAM), list_items, useDetails=not is_compact)
        if ret < 0:
            return None
        return streams[ret]

    @staticmethod
    def keyboard(title, hidden=False):
        keyboard = xbmc.Keyboard('', title, hidden)
        keyboard.doModal()
        if keyboard.isConfirmed():
            return keyboard.getText().strip()
        else:
            return None

    @staticmethod
    def ok(heading, *args, **kwargs):
        return Dialog().ok(heading, *args, **kwargs)

    @staticmethod
    def ok_multi_line(heading, lines):
        return DialogRenderer.ok(heading, STRINGS.NEW_LINE.join(lines))

    @staticmethod
    def progress(heading, *args, **kwargs):
        d = DialogProgress()
        d.create(heading, *args, **kwargs)
        return d

    @staticmethod
    def select(heading, choices, **kwargs):
        return select(heading, choices, **kwargs)

    @staticmethod
    def yesno(heading, *args, **kwargs):
        return Dialog().yesno(heading, *args, **kwargs)

    @staticmethod
    def info_invalid_credentials():
        InfoDialog(get_string(LANG.INCORRECT_PROVIDER_CREDENTIALS), sound=True).notify()

    @staticmethod
    def set_subtitles_credentials(provider):
        username = DialogRenderer.keyboard(get_string(LANG.USERNAME))
        if username:
            time.sleep(1)  # tvOS fix
            password = DialogRenderer.keyboard(get_string(LANG.PASSWORD), hidden=True)
            if password:
                if not Subtitles.set_credentials(provider, username, password):
                    DialogRenderer.info_invalid_credentials()
