import contextlib

import xbmcplugin

from resources.lib.api.api import API
from resources.lib.api.trakt_api import trakt
from resources.lib.compatibility import encode_utf, decode_utf
from resources.lib.const import COMMAND, ROUTE, LANG, SETTINGS, STRINGS, MEDIA_TYPE, DIR_TYPE, \
    ICON, RENDER_TYPE, FILTER, COUNT, MENU_ITEM, COLOR, lang_media_type
from resources.lib.gui import MoviesItem, SettingsItem, SearchItem, DirectoryItem, TvShowsItem, WatchHistoryItem, \
    MainMenuFolderItem, DownloadQueueItem, TraktItem, SearchHistoryItem, CommandItem, TvProgramItem, DatePickerItem, \
    FilterItem
from resources.lib.gui.renderers import Renderer
from resources.lib.gui.renderers.dialog_renderer import DialogRenderer
from resources.lib.gui.text_renderer import DateRenderer, TextRenderer
from resources.lib.kodilogging import logger
from resources.lib.routing.router import router
from resources.lib.storage.settings import settings
from resources.lib.storage.sqlite import DB
from resources.lib.utils.kodiutils import get_string, get_icon, languages_gui, parse_date, pretty_date_ago, colorize


class Directory:
    @staticmethod
    def main_menu():

        menu = [
            SearchItem(url=router.get_url(ROUTE.COMMAND, command=COMMAND.SEARCH)),
        ]
        if settings[SETTINGS.SEARCH_HISTORY]:
            menu.append(SearchHistoryItem(url=router.get_url(ROUTE.SEARCH_HISTORY)))

        menu.extend([
            MoviesItem(url=router.get_url(ROUTE.MOVIES)),
            TvShowsItem(url=router.get_url(ROUTE.TV_SHOWS)),
            TvProgramItem(url=router.get_url(ROUTE.TV_STATIONS)),
        ])

        if trakt.is_authenticated():
            menu.append(TraktItem())

        if settings[SETTINGS.PLAYED_ITEMS_HISTORY]:
            menu.append(WatchHistoryItem(url=router.get_url(ROUTE.WATCHED), key=MENU_ITEM.WATCHED))

        menu.extend([
            DownloadQueueItem(url=router.get_url(ROUTE.DOWNLOAD_QUEUE), key=MENU_ITEM.DOWNLOAD_QUEUE),
            SettingsItem(url=router.get_url(ROUTE.COMMAND, command=COMMAND.OPEN_SETTINGS))
        ])

        return menu

    @staticmethod
    def movies():
        return Directory.media_menu(MEDIA_TYPE.MOVIE, [
            Directory.MENU_ITEM.csfd_tips(MEDIA_TYPE.MOVIE)
        ])

    @staticmethod
    def watched_new():
        media_types = DB.WATCH_HISTORY.get_media_types()
        all_history = Directory.MENU_ITEM.watched_all()
        menu_items = [all_history]
        for media_type in media_types:
            menu_item = WATCH_HISTORY_MAP.get(media_type[0])
            if menu_item:
                menu_items.append(menu_item())
        return menu_items

    @staticmethod
    def tv_shows():
        return Directory.media_menu(MEDIA_TYPE.TV_SHOW, [])

    @staticmethod
    def media_menu(media_type, additional_items):
        return [
                   Directory.MENU_ITEM.trending(media_type),
                   Directory.MENU_ITEM.popular(media_type),
                   Directory.MENU_ITEM.most_watched(media_type),
                   Directory.MENU_ITEM.new_releases(media_type),
                   Directory.MENU_ITEM.new_releases_dubbed(media_type),
                   Directory.MENU_ITEM.last_added(media_type),

               ] + additional_items + Directory.by_menu(media_type)

    @staticmethod
    def by_menu(media_type):
        return [Directory.MENU_ITEM.a_z(media_type),
                Directory.MENU_ITEM.genres(media_type),
                Directory.MENU_ITEM.countries(media_type),
                Directory.MENU_ITEM.languages(media_type),
                Directory.MENU_ITEM.years(media_type),
                Directory.MENU_ITEM.studios(media_type), ]

    @staticmethod
    def count_menu(data_count, icon_builder, context_menu, url_builder, media_type, dir_type, count_type):
        list_items = []
        for item in data_count:
            key = item.get('key')
            list_items.append(DirectoryItem(key=key,
                                            title=item.get('title'),
                                            context_menu=[
                                                menu_item(ROUTE.COUNT_MENU, key, media_type, dir_type, count_type) for
                                                menu_item in
                                                context_menu],
                                            icon=icon_builder(key),
                                            url=url_builder(item)))
        return list_items

    @staticmethod
    def count_menu_next_page(media_type, count_type, filter_type, render_type, filter_value=STRINGS.EMPTY, page=0):
        return DirectoryItem(title=get_string(LANG.NEXT_PAGE),
                             icon=get_icon(ICON.NEXT),
                             url=router.get_url(ROUTE.COUNT_MENU,
                                                page=page,
                                                count_type=count_type,
                                                filter_type=filter_type,
                                                filter_value=filter_value,
                                                media_type=media_type,
                                                render_type=render_type),
                             )

    class MENU_ITEM:
        @staticmethod
        def _description(lang):
            return get_string(lang)

        @staticmethod
        def watched_all(*args):
            return WatchHistoryItem(url=router.get_url(ROUTE.WATCHED, show_all=True), key=MENU_ITEM.WATCHED, title=get_string(LANG.WATCH_HISTORY_ALL))

        @staticmethod
        def watched_movies(*args):
            return DirectoryItem(title=get_string(LANG.MOVIES),
                                 icon=get_icon(ICON.MOVIES),
                                 key=MENU_ITEM.WATCHED_MOVIES,
                                 media_type=MEDIA_TYPE.MOVIE,
                                 url=router.get_url(ROUTE.WATCHED, media_type=MEDIA_TYPE.MOVIE))

        @staticmethod
        def watched_tv_shows(*args):
            return DirectoryItem(title=get_string(LANG.TV_SHOWS),
                                 icon=get_icon(ICON.TVSHOWS),
                                 key=MENU_ITEM.WATCHED_TV_SHOWS,
                                 media_type=MEDIA_TYPE.MOVIE,
                                 url=router.get_url(ROUTE.WATCHED, media_type=MEDIA_TYPE.TV_SHOW))

        @staticmethod
        def csfd_tips(media_type):
            return DirectoryItem(title=get_string(LANG.CSFD_TIPS),
                                 icon=get_icon(ICON.TV),
                                 key=MENU_ITEM.CSFD_TIPS,
                                 url=router.get_url(ROUTE.CSFD_TIPS))

        @staticmethod
        def count_dir(media_type, translation_id, icon, count_type, filter_type, render_type, key,
                      filter_value=STRINGS.EMPTY, page=0, **kwargs):
            return DirectoryItem(title=get_string(translation_id),
                                 icon=get_icon(icon),
                                 key=key,
                                 url=router.get_url(ROUTE.COUNT_MENU,
                                                    page=page,
                                                    count_type=count_type,
                                                    filter_type=filter_type,
                                                    filter_value=filter_value,
                                                    media_type=media_type,
                                                    render_type=render_type,
                                                    **kwargs),
                                 )

        @staticmethod
        def a_z(media_type, title=LANG.A_Z, icon=ICON.A_Z, digits=0):
            return Directory.MENU_ITEM.count_dir(media_type, title, icon, COUNT.TITLES,
                                                 FILTER.STARTS_WITH_SIMPLE,
                                                 RENDER_TYPE.A_Z,
                                                 MENU_ITEM.A_Z,
                                                 digits=digits)

        @staticmethod
        def studios(media_type):
            return Directory.MENU_ITEM.count_dir(media_type, LANG.BY_STUDIO, ICON.STUDIO, COUNT.STUDIOS,
                                                 FILTER.STUDIO,
                                                 RENDER_TYPE.DEFAULT, MENU_ITEM.STUDIOS)

        @staticmethod
        def genres(media_type):
            return Directory.MENU_ITEM.count_dir(media_type, LANG.GENRE, ICON.GENRE, COUNT.GENRES, FILTER.GENRE,
                                                 RENDER_TYPE.DEFAULT, MENU_ITEM.GENRES)

        @staticmethod
        def countries(media_type):
            return Directory.MENU_ITEM.count_dir(media_type, LANG.BY_COUNTRY, ICON.COUNTRY, COUNT.COUNTRIES,
                                                 FILTER.COUNTRY,
                                                 RENDER_TYPE.DEFAULT, MENU_ITEM.COUNTRIES)

        @staticmethod
        def languages(media_type):
            return Directory.MENU_ITEM.count_dir(media_type, LANG.BY_LANGUAGE, ICON.LANGUAGE, COUNT.LANGUAGES,
                                                 FILTER.LANGUAGE,
                                                 RENDER_TYPE.DEFAULT, MENU_ITEM.LANGUAGES)

        @staticmethod
        def years(media_type):
            return Directory.MENU_ITEM.count_dir(media_type, LANG.BY_YEAR, ICON.YEAR, COUNT.YEARS, FILTER.YEAR,
                                                 RENDER_TYPE.DEFAULT, MENU_ITEM.YEARS)

        @staticmethod
        def new_releases(media_type):
            return DirectoryItem(title=get_string(LANG.NEWS),
                                 icon=get_icon(ICON.NEW),
                                 key=MENU_ITEM.NEW_RELEASES,
                                 url=router.get_media(media_type, RENDER_TYPE.DEFAULT,
                                                      API.FILTER.new_releases(media_type)))

        @staticmethod
        def new_releases_dubbed(media_type, languages=settings.get_languages(SETTINGS.PREFERRED_LANGUAGE,
                                                                             SETTINGS.FALLBACK_LANGUAGE)):
            title = get_string(LANG.NEWS_DUBBED)
            title = STRINGS.NEWS_DUBBED_DIR_TITLE.format(title=title, languages='/'.join(languages_gui(languages)))
            return DirectoryItem(title=title,
                                 icon=get_icon(ICON.NEW_DUB),
                                 key=MENU_ITEM.NEW_RELEASES_DUBBED,
                                 url=router.get_media(media_type,
                                                      RENDER_TYPE.DEFAULT,
                                                      API.FILTER.new_releases_dubbed(media_type, languages)))

        @staticmethod
        def most_watched(media_type):
            return DirectoryItem(title=get_string(LANG.MOST_WATCHED),
                                 icon=get_icon(ICON.MOST_WATCHED),
                                 key=MENU_ITEM.MOST_WATCHED,
                                 description=Directory.MENU_ITEM._description(LANG.DESC_MOST_WATCHED),
                                 url=router.get_media(media_type,
                                                      RENDER_TYPE.DEFAULT,
                                                      API.FILTER.most_watched(media_type)))

        @staticmethod
        def popular(media_type):
            return DirectoryItem(title=get_string(LANG.POPULAR),
                                 icon=get_icon(ICON.POPULAR),
                                 key=MENU_ITEM.POPULAR,
                                 description=Directory.MENU_ITEM._description(LANG.DESC_POPULAR),
                                 url=router.get_media(media_type,
                                                      RENDER_TYPE.DEFAULT,
                                                      API.FILTER.popular(media_type)))

        @staticmethod
        def trending(media_type):
            return DirectoryItem(title=get_string(LANG.TRENDING),
                                 icon=get_icon(ICON.TRENDING),
                                 key=MENU_ITEM.TRENDING,
                                 description=Directory.MENU_ITEM._description(LANG.DESC_TRENDING),
                                 url=router.get_media(media_type,
                                                      RENDER_TYPE.DEFAULT,
                                                      API.FILTER.trending(media_type)))

        @staticmethod
        def last_added(media_type):
            return DirectoryItem(title=get_string(LANG.LAST_ADDED),
                                 icon=get_icon(ICON.RECENT),
                                 key=MENU_ITEM.LAST_ADDED,
                                 url=router.get_media(media_type,
                                                      RENDER_TYPE.DEFAULT,
                                                      API.FILTER.last_added(media_type)))

        @staticmethod
        def hidden_items(media_type, route):
            return DirectoryItem(title='[COLOR lightgray][I]' + get_string(LANG.HIDDEN) + '[/I][/COLOR]',
                                 url=router.get_url(ROUTE.HIDDEN_MENU_ITEMS,
                                                    dir_route=route,
                                                    media_type=media_type))

        @staticmethod
        def previous_next_date(date, from_date, to_date):
            return DirectoryItem(title=get_string(LANG.PREVIOUS),
                                 url=router.get_url(ROUTE.TV_STATIONS, selected_date=date, from_date=from_date,
                                                    to_date=to_date))


MENU_ITEM_MAP = {
    MENU_ITEM.A_Z: Directory.MENU_ITEM.a_z,
    MENU_ITEM.STUDIOS: Directory.MENU_ITEM.studios,
    MENU_ITEM.GENRES: Directory.MENU_ITEM.genres,
    MENU_ITEM.COUNTRIES: Directory.MENU_ITEM.countries,
    MENU_ITEM.LANGUAGES: Directory.MENU_ITEM.languages,
    MENU_ITEM.YEARS: Directory.MENU_ITEM.years,
    MENU_ITEM.NEW_RELEASES: Directory.MENU_ITEM.new_releases,
    MENU_ITEM.NEW_RELEASES_DUBBED: Directory.MENU_ITEM.new_releases_dubbed,
    MENU_ITEM.POPULAR: Directory.MENU_ITEM.popular,
    MENU_ITEM.TRENDING: Directory.MENU_ITEM.trending,
    MENU_ITEM.LAST_ADDED: Directory.MENU_ITEM.last_added,
    MENU_ITEM.CSFD_TIPS: Directory.MENU_ITEM.csfd_tips,
    MENU_ITEM.MOST_WATCHED: Directory.MENU_ITEM.most_watched,
    MENU_ITEM.WATCHED_MOVIES: Directory.MENU_ITEM.watched_movies,
    MENU_ITEM.WATCHED_TV_SHOWS: Directory.MENU_ITEM.watched_tv_shows,
    MENU_ITEM.WATCHED: Directory.MENU_ITEM.watched_all,
}

WATCH_HISTORY_MAP = {
    MEDIA_TYPE.MOVIE: Directory.MENU_ITEM.watched_movies,
    MEDIA_TYPE.TV_SHOW: Directory.MENU_ITEM.watched_tv_shows,
}


class DirectoryRenderer(Renderer):
    def __init__(self):
        super(DirectoryRenderer, self).__init__()

    def __call__(self, handle, list_items, route=None, media_type=None, dir_type=DIR_TYPE.NONE):
        pinned_items = [row[0] for row in DB.PINNED.get(route)] if route else []
        hidden_items = [row[0] for row in DB.HIDDEN.get(route)] if route else []
        DirectoryRenderer.render(handle, list_items, pinned_items, hidden_items, route, media_type, dir_type)

    @staticmethod
    @contextlib.contextmanager
    def start_directory(handle, as_type=DIR_TYPE.NONE):
        """Simple context manager that automatically ends the directory."""
        xbmcplugin.setContent(handle, as_type)
        yield
        DirectoryRenderer.end_directory(handle)

    @staticmethod
    def end_directory(handle):
        xbmcplugin.endOfDirectory(handle, cacheToDisc=False)

    @staticmethod
    def a_to_z_menu(media_type, count_menu, letters, letter_counts, digits):
        if letters:
            count_menu.insert(0, MainMenuFolderItem(url=router.get_url(ROUTE.CLEAR_PATH)))
            search_title = get_string(LANG.SEARCH_FOR_LETTERS).format(letters=letters)
            count_menu.insert(0, DirectoryItem(
                icon=get_icon(ICON.SEARCH),
                title=DirectoryRenderer.TITLE.count({'title': search_title, 'doc_count': letter_counts.get('total')}),
                url=router.get_media(media_type,
                                     RENDER_TYPE.A_Z,
                                     API.FILTER.a_z(media_type, letters),
                                     letters=letters)
            ))
            count_menu.append(MainMenuFolderItem(url=router.get_url(ROUTE.CLEAR_PATH)))
        elif not int(digits) and not letters:
            count_menu.insert(0, Directory.MENU_ITEM.a_z(media_type, LANG.NUMBERS, ICON.NUMBERS, digits=1))
        return count_menu

    @staticmethod
    def search_history_menu(media_type, search_items):
        menu = []
        for item in search_items:
            value = item[0]
            last_played = parse_date(item[1])
            count = item[2]
            ago = colorize(COLOR.GREY, pretty_date_ago(last_played))
            menu.append(DirectoryItem(title=STRINGS.HISTORY_MENU_TITLE.format(encode_utf(value), ago, count),
                                      icon=get_icon(ICON.SEARCH_HISTORY_1),
                                      url=router.get_media(media_type,
                                                           RENDER_TYPE.SEARCH,
                                                           API.FILTER.search(MEDIA_TYPE.ALL, encode_utf(value)))))
        return menu

    @staticmethod
    def tv_program(media_type, stations, selected_date, min_date, max_date):

        menu = [MainMenuFolderItem(url=router.get_url(ROUTE.CLEAR_PATH)),
                DatePickerItem(title=STRINGS.CHOOSE_DATE % (TextRenderer.bold(get_string(LANG.CHOOSE_DATE)),
                                                            TextRenderer.bold(str(DateRenderer(selected_date)))),
                               url=router.get_url(ROUTE.COMMAND, command=COMMAND.CHOOSE_DATE, min_date=min_date,
                                                  max_date=max_date, selected_date=selected_date)),
                FilterItem(
                    title=STRINGS.PAIR.format(get_string(LANG.MEDIA_TYPE), get_string(lang_media_type.get(media_type))),
                    url=router.get_url(ROUTE.COMMAND, command=COMMAND.INCREMENT_ENUM,
                                       setting_name=SETTINGS.TV_PROGRAM_MEDIA_TYPE))

                ]
        if media_type == MEDIA_TYPE.TV_SHOW:
            media_type = MEDIA_TYPE.EPISODE
        for station in stations:
            menu.append(
                DirectoryItem(title=station.get_title(),
                              key=station.name,
                              icon=station.logo,
                              url=router.get_media(media_type,
                                                   RENDER_TYPE.TV,
                                                   API.FILTER.tv_program(media_type, selected_date, station.name),
                                                   station_name=station.name, date=selected_date)))

        menu.insert(2, CommandItem(title=get_string(LANG.SHOW_ALL_STATIONS),
                                   url=router.get_url(ROUTE.COMMAND,
                                                      command=COMMAND.CHOOSE_TIME, media_type=media_type,
                                                      selected_date=selected_date)
                                   ))
        return menu

    @staticmethod
    def render(handle, list_items, pinned_items, hidden_items, route, media_type, dir_type):
        with DirectoryRenderer.start_directory(handle, dir_type):
            before_items = []
            list_items_with_key = []
            after_items = []
            append_before = True
            for item in list_items:
                if item.key:
                    append_before = False
                    list_items_with_key.append(item)
                elif append_before:
                    before_items.append(item)
                else:
                    after_items.append(item)
            list_items_with_key.sort(key=DirectoryRenderer._sort_pinned(pinned_items))
            list_items = before_items + list_items_with_key + after_items
            for item in list_items:
                key = str(item.key)
                if key in hidden_items:
                    continue
                if decode_utf(key) in pinned_items:
                    item.title = STRINGS.PINNED_ITEM.format(item=item.title)
                item(handle)
            if len(hidden_items) > 0 and settings[SETTINGS.SHOW_HIDDEN]:
                Directory.MENU_ITEM.hidden_items(media_type, route)(handle)

    @staticmethod
    def _sort_pinned(pinned):
        def helper(item):
            key = decode_utf(str(item.key))
            if key in pinned:
                return pinned.index(key)
            return len(pinned)

        return helper

    class URL:
        @staticmethod
        def default(media_type, filter_name, render_type, count_type, item, **kwargs):
            key = item.get('key')
            return router.get_media(media_type,
                                    render_type,
                                    API.FILTER.filter(media_type, filter_name, key), **kwargs)

        @staticmethod
        def az(media_type, filter_type, render_type, count_type, item, **kwargs):
            key = item.get('key')
            if item.get('doc_count') <= settings[SETTINGS.A_Z_THRESHOLD]:
                return DirectoryRenderer.URL.default(media_type, filter_type, render_type, count_type, item,
                                                     letters=key)
            else:
                return router.get_url(ROUTE.COUNT_MENU,
                                      count_type=count_type,
                                      filter_type=filter_type,
                                      filter_value=key,
                                      media_type=media_type,
                                      render_type=render_type,
                                      **kwargs
                                      )

    class TITLE:
        @staticmethod
        def default(item):
            title = item.get('title')
            return title

        @staticmethod
        def count(item):
            title = item.get('title')
            count = item.get('doc_count')
            return STRINGS.COUNT_TITLE.format(title, str(count))

        @staticmethod
        def pinned(pinned_keys, item):
            title = item.get('title')
            key = item.get('key')
            if key in pinned_keys:
                return STRINGS.PINNED_ITEM.format(item=title)
            return title

    # Cannot be more than 1 dir deep due to path history reset
    @staticmethod
    def search(media_type):
        logger.debug('Search dialog opened')
        # xbmcplugin.endOfDirectory(handle, cacheToDisc=False)
        search_value = DialogRenderer.search()
        if search_value:
            router.go(router.get_media(media_type,
                                       RENDER_TYPE.SEARCH,
                                       API.FILTER.search(media_type, search_value)))
