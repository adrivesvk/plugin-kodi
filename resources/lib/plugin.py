import json
import time
from socket import error as socket_error

import xbmc

from resources.lib.communication.socket_wrapper import SocketClient
from resources.lib.const import LANG
from resources.lib.gui.renderers.dialog_renderer import DialogRenderer
from resources.lib.gui.renderers.directory_renderer import DirectoryRenderer
from resources.lib.kodilogging import logger, setup_root_logger
from resources.lib.utils.kodiutils import get_string

socket_client = SocketClient()
tries = 300


def stop_if_updating(handle):
    if not SocketClient.address_exists():
        logger.info('Service unavailable. It is being updated.')
        DialogRenderer.ok(get_string(LANG.UPDATE_IN_PROGRESS), get_string(LANG.PLUGIN_UNAVAILABLE_TRY_LATER))
        DirectoryRenderer.end_directory(handle)
        xbmc.executebuiltin("XBMC.ActivateWindow(Home)")
        return True


# @olii had really good idea of keeping whole plugin in service and use plugin only to tell service what to do
def run(argv):
    setup_root_logger()
    url, handle, query = argv[0], int(argv[1]), argv[2]
    address = None
    for i in range(tries):
        if stop_if_updating(handle):
            return
        try:
            address = socket_client.get_address()
            if address:
                response, address = socket_client.send('handshake', address)
                if response == b'love you':
                    break
        except socket_error as e:
            logger.error(e)
        logger.info('Socket did not respond. Retrying... %s/%s' % (i, tries))
        time.sleep(0.1)
    if address:
        socket_client.settimeout(None)
        socket_client.send(json.dumps({'handle': handle, 'url': url, 'query': query}), address)
