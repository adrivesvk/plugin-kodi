from threading import Thread

from resources.lib.const import SERVICE, SETTINGS, DOWNLOAD_SERVICE_EVENT, DOWNLOAD_STATUS
from resources.lib.kodilogging import logger
from resources.lib.services import ThreadService
from resources.lib.storage.sqlite import SQLiteStorage, DB
from resources.lib.utils.download import download
from resources.lib.storage.settings import settings


class DownloadService(ThreadService):
    SERVICE_NAME = SERVICE.DOWNLOAD_SERVICE

    def __init__(self, api):
        super(DownloadService, self).__init__()
        self._api = api
        self._event_callbacks = {
            DOWNLOAD_SERVICE_EVENT.ITEM_ADDED: self.process_queue,
            DOWNLOAD_SERVICE_EVENT.ITEM_RESUMED: self.resume_download_item
        }
        self.db = SQLiteStorage.Download()

    def start_up(self):
        DB.DOWNLOAD.change_status(DOWNLOAD_STATUS.DOWNLOADING, DOWNLOAD_STATUS.QUEUED)      # Fix for stucked 'downloading' records in DB, when Kodi/SCC processes hung/restart
        self.process_queue()
    
    def process_queue(self):
        queue = self.db.get_queued()
        max_down = settings[SETTINGS.DOWNLOADS_PARALLEL]
        if len(queue) > 0:
            logger.debug("Something in queue")
            downloading = len(self.db.get_downloading())
            if max_down > downloading:
                queue = self.db.get_queued(max_down-downloading)
                for row in queue:
                    self.download(row)
                    logger.debug("Downloading from queue")
    
    def resume_download_item(self, download_id):
        item = DB.DOWNLOAD.get(download_id)
        max_down = settings[SETTINGS.DOWNLOADS_PARALLEL]
        downloading = len(self.db.get_downloading())
        logger.debug("Resume downloading of item")
        if max_down > downloading:
            self.download(item)
        else: 
            logger.debug("No slot for specific resume. Must wait in queue")
            self.process_queue()

    def download(self, item):
        dl_id = item[0]
        download_folder = item[1]
        url = item[2]
        name = item[3]
        worker = Thread(target=download, args=(url, download_folder, name, dl_id))
        self.threads.append(worker)
        worker.start()
