from resources.lib.const import SETTINGS, ROUTE
from resources.lib.kodilogging import service_logger
from resources.lib.services import Service
from resources.lib.storage.settings import settings
from resources.lib.utils.kodiutils import refresh


# settings_actions = {
#     SETTINGS.USE_LIBRARY: library_service_control
# }

refresh_map = {
    ROUTE.ROOT: True
}


class SettingsService(Service):
    def __init__(self, routing_service):
        super(SettingsService, self).__init__()
        self.settings = settings
        self.routing_service = routing_service

    def onSettingsChanged(self):
        old_settings = self.settings.current_values.copy()
        self.settings.load()
        for k, v in old_settings.items():
            if v != self.settings.current_values[k]:
                service_logger.debug('Setting changed. Refreshing.')
                # settings_actions.get(k, lambda x: x)(v)
                if refresh_map.get(self.routing_service.previous_url_no_query):
                    refresh()
                break
