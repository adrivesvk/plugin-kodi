import os
import re

from resources.lib.api.api import API
from resources.lib.compatibility import HTTPError, encode_utf, decode_utf
from resources.lib.const import SETTINGS, STRINGS, HTTP_METHOD, LANG, ACTION, DOWNLOAD_STATUS, trakt_type_map
from resources.lib.defaults import Defaults
from resources.lib.gui import InfoDialog, MediaItem
from resources.lib.gui.renderers.dialog_renderer import DialogRenderer
from resources.lib.gui.renderers.media_info_renderer import MediaInfoRenderer
from resources.lib.kodilogging import logger
from resources.lib.routing.router import router
from resources.lib.storage.settings import settings
from resources.lib.storage.sqlite import DB
from resources.lib.subtitles import Subtitles
from resources.lib.utils import streams
from resources.lib.utils.addtolib import normalize_name
from resources.lib.utils.kodiutils import get_string, refresh, convert_bitrate, swap_keys_with_values
from resources.lib.utils.url import Url
from resources.lib.wrappers.http import Http


class StreamPicker:
    def __init__(self, plugin_core):
        self._plugin_core = plugin_core

        self.permission_map_playback = {
            True: self.process_stream,
            False: self.login_process_stream
        }

        self.stream_action_map = {
            ACTION.PLAY: self.play_stream,
            ACTION.DOWNLOAD: self.download_stream,
        }

    def stream_guard(self, *args):
        self.permission_map_playback[settings[SETTINGS.PROVIDER_LOGGED_IN]](*args)

    def process_stream(self, handle, stream, media_id, root_parent_id, action):
        if self._plugin_core.auth.check_account():
            self.stream_action_map[action](handle, stream, media_id, root_parent_id)
        else:
            router.set_resolved_url(handle)

    def login_process_stream(self, handle, stream, media_id, root_parent_id, action):
        if self._plugin_core.auth.set_provider_credentials():
            self.process_stream(handle, stream, media_id, root_parent_id, action)
        else:
            router.set_resolved_url(handle)

    def download_stream(self, handle, stream, media_id, root_parent_id):
        if settings[SETTINGS.DOWNLOADS_FOLDER] == '':
            InfoDialog(get_string(LANG.DL_DIR_NOT_SET), sound=True).notify()
            return
        stream_url = self.get_stream_url(handle, stream)
        if not stream_url:
            return
        a = Url.urlparse(stream_url)
        filename = os.path.basename(a.path)
        original_title, original_folder = self.title_for_download(media_id)
        if original_title:
            filename = original_title + '_' + filename
        if not re.search(r'avi$|mkv$|ts$|htm$|html$|php$|mp4$|mpg$|mpeg$', filename):
            filename += '.mkv'
        dl_item = DB.DOWNLOAD.get_by_filename(original_folder, filename)
        if dl_item:
            status = dl_item[4]
            if status == DOWNLOAD_STATUS.CANCELLED or status == DOWNLOAD_STATUS.COMPLETED or status == DOWNLOAD_STATUS.PAUSED:
                DB.DOWNLOAD.set_status(dl_item[0], DOWNLOAD_STATUS.QUEUED)
                self.download()
                refresh()
                return
            elif status == DOWNLOAD_STATUS.QUEUED or status == DOWNLOAD_STATUS.DOWNLOADING:
                InfoDialog(get_string(LANG.ALREADY_DOWNLOADING)).notify()
                return
        InfoDialog(get_string(LANG.STARTING_DOWNLOAD).format(filename=original_title)).notify()
        self.download_new(stream_url, original_folder, filename, media_id)

    def download_new(self, url, download_folder, filename, media_id):
        DB.DOWNLOAD.add(url, decode_utf(download_folder), decode_utf(filename), media_id)
        self.download()

    def download(self):
        self._plugin_core.download_service.process_queue()

    def get_streams(self, url):
        response = self._plugin_core.api_request(HTTP_METHOD.GET, url)
        if len(response) == 0:
            msg = get_string(LANG.MISSING_STREAM_TEXT) + ' ' + get_string(LANG.TELL_US_ABOUT_IT)
            DialogRenderer.ok(get_string(LANG.MISSING_STREAM_TITLE), msg)
            return
        return response

    def get_stream_url(self, handle, stream):
        stream_url = self._plugin_core.provider.get_link_for_file_with_id(stream.get('ident'))
        if not stream_url:
            self._plugin_core.api.report_stream(stream.get('_id'))
            msg = get_string(LANG.STREAM_NOT_AVAILABLE) + ' ' + get_string(LANG.STREAM_FLAGGED_UNAVAILABLE)
            DialogRenderer.ok(get_string(LANG.MISSING_STREAM_TITLE), msg)
            router.set_resolved_url(handle)
        else:
            r = Http.head(stream_url)
            if '?error=' in stream_url or '?error=' in r.url:
                DialogRenderer.ok(get_string(LANG.PROVIDER_DIDNT_RETURN_VALID_LINK),
                                  get_string(LANG.SOMEONE_ELSE_IS_WATCHING))
                router.set_resolved_url(handle)
                return
        return stream_url

    def play_stream(self, handle, stream, media_id, root_parent_id):
        logger.debug('Trying to play stream.')
        stream_url = self.get_stream_url(handle, stream)
        if stream_url:
            media = self._plugin_core.api_request(HTTP_METHOD.GET, API.URL.media_detail(media_id))
            self.play(handle, media, stream, stream_url, root_parent_id)

    def play(self, handle, media, stream, stream_url, root_parent_id):
        langs = MediaInfoRenderer.get_language_priority()
        labels, art = MediaInfoRenderer.merge_info_labels(media, langs)
        media_type = labels.get('mediatype')
        trakt_id = media.get('services', {}).get('trakt')
        if trakt_id:
            type_map = swap_keys_with_values(trakt_type_map)
            trakt_id = {type_map[media_type]: {'ids': {'trakt': trakt_id}}}
        title = labels.get('title')

        subtitles_string = None
        has_preferred_subs = self._plugin_core.has_preferred('subtitles', SETTINGS.SUBTITLES_PROVIDER_PREFERRED_LANGUAGE, stream)
        if not has_preferred_subs and settings[SETTINGS.SUBTITLES_PROVIDER_AUTO_SEARCH]:
            logger.debug('Subtitles are missing. Lets create string search.')
            subtitles_string = Subtitles.build_search_string(labels)

        url, item, directory = MediaItem(title, url=stream_url, cast=media.get('cast'), art=art,
                                         info_labels=labels).build()

        logger.debug('Stream URL found. Playing %s' % url)
        self._plugin_core.player.play(handle, item, url, media_id=root_parent_id, trakt_id=trakt_id, media=media,
                                      sub_strings=subtitles_string)

    @staticmethod
    def folder_for_download(info_labels):
        """
        This return folder with standard naming format, same as addtolib, to allow Kodi scrappers find relevant informations if needed
        """
        episode = info_labels.get('episode')
        if episode:
            # TODO: Use original tvshowtitle for string, after it will be avaible inside child
            folder = STRINGS.SUBTITLES_SEARCH_TITLE.format(title=MediaInfoRenderer.get_root_title(info_labels) or MediaInfoRenderer.get_root_title(info_labels['labels_eng']), year=str(info_labels.get('year')))
        else:
            title = info_labels.get('originaltitle', info_labels.get('sorttitle')) or info_labels.get(
                'title') or info_labels.get('labels_eng', {}).get('title', 'N/A')
            folder = STRINGS.SUBTITLES_SEARCH_TITLE.format(title=encode_utf(title), year=str(info_labels.get('year')))
        return folder
    
    @staticmethod
    def title_for_download(media_id):
        res = ''
        try:
            res = Defaults.api().request(HTTP_METHOD.GET, API.URL.media_detail(media_id))
        except HTTPError as err:
            logger.error(err)
        if res != '':
            media_detail = res.json()
            langs = MediaInfoRenderer.get_language_priority()
            labels, art = MediaInfoRenderer.merge_info_labels(media_detail, langs)
            download_title = encode_utf(normalize_name(decode_utf(MediaInfoRenderer.TITLE.subtitles_string(labels))))
            download_folder = encode_utf(normalize_name(decode_utf(StreamPicker.folder_for_download(labels))))
            if settings[SETTINGS.DOWNLOADS_FOLDER_LIBRARY] and settings[SETTINGS.MOVIE_LIBRARY_FOLDER] and settings[SETTINGS.TV_SHOW_LIBRARY_FOLDER]:  # due to lack of settings cross-referency check we need to be sure. (https://github.com/xbmc/xbmc/issues/15252)
                if labels.get('episode'): download_folder = os.path.join(settings[SETTINGS.TV_SHOW_LIBRARY_FOLDER], download_folder, 'Season '+str(labels.get('season')).zfill(2))
                else: download_folder = os.path.join(settings[SETTINGS.MOVIE_LIBRARY_FOLDER], download_folder)
            else:  
                if labels.get('episode'): download_folder = os.path.join(settings[SETTINGS.DOWNLOADS_FOLDER], download_folder, 'Season '+str(labels.get('season')).zfill(2))
                else: download_folder = os.path.join(settings[SETTINGS.DOWNLOADS_FOLDER], download_folder)
            return download_title, download_folder

    @staticmethod
    def choose_stream(stream_list):
        return DialogRenderer.choose_video_stream(stream_list)

    def select_stream(self, stream_list, force, action):
        selected_stream = None
        if (settings[SETTINGS.STREAM_AUTOSELECT] and not settings[SETTINGS.STREAM_AUTOSELECT_DOWNLOAD] and force == '0') or (action == ACTION.DOWNLOAD and settings[SETTINGS.STREAM_AUTOSELECT_DOWNLOAD]):
            selected_stream = self._choose_stream_auto(stream_list)

        if selected_stream is None:
            selected_stream = self.choose_stream(stream_list)
        return selected_stream

    def get_and_select_stream(self, url, force, action):
        stream_list = self.get_streams(url)
        if stream_list:
            return self.select_stream(StreamPicker.filter_streams(stream_list), force, action)

    @staticmethod
    def is_hdr(video):
        return video.get('hdr')

    @staticmethod
    def is_3d(video):
        return video.get('3d')

    @staticmethod
    def filter_streams(stream_list):
        filtered_list = []
        for stream in stream_list:
            video = stream.get('video', [])
            if len(video) == 0:
                continue

            video = video[0]
            if not settings[SETTINGS.SHOW_HDR_STREAMS] and StreamPicker.is_hdr(video):
                continue

            if not settings[SETTINGS.SHOW_3D_STREAMS] and StreamPicker.is_3d(video):
                continue

            if video.get('duration') and stream.get('size'):
                bit_rate = stream.get('size') / video.get('duration') * 8
                max_bit_rate = settings[SETTINGS.STREAM_MAX_BITRATE_READABLE]
                if settings[SETTINGS.MAX_BITRATE_FILTER]:
                    bit_rate = convert_bitrate(bit_rate, False)
                    if 0 < max_bit_rate < bit_rate < 200:
                        continue
            filtered_list.append(stream)
        return filtered_list

    @staticmethod
    def _choose_stream_auto(response):
        preferred_quality = settings[SETTINGS.STREAM_AUTOSELECT_PREFERRED_QUALITY]
        preferred_languages = [settings[SETTINGS.PREFERRED_LANGUAGE], settings[SETTINGS.FALLBACK_LANGUAGE]]
        max_bitrate = settings[SETTINGS.STREAM_AUTOSELECT_MAX_BITRATE] if settings[SETTINGS.STREAM_AUTOSELECT_LIMIT_BITRATE] else 0
        avoid_vcodecs = (settings[SETTINGS.STREAM_AUTOSELECT_AVOID_VIDEO_CODECS] or '').split(',')
        preffered_vcodecs = (settings[SETTINGS.STREAM_AUTOSELECT_PREFERRED_VIDEO_CODECS] or '').split(',')
        preferred_channels = settings[SETTINGS.STREAM_AUTOSELECT_PREFERRED_CHANNELS] if settings[SETTINGS.STREAM_AUTOSELECT_PREFERRED_CHANNELS_ENABLED] else None
        return streams.select(response,
                              audio=preferred_languages,
                              max_bitrate=max_bitrate,
                              vquality=preferred_quality,
                              channels=preferred_channels,
                              preffered_vcodecs=preffered_vcodecs,
                              codec_blacklist=avoid_vcodecs)
