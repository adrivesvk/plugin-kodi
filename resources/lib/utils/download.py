# -*- coding: utf-8 -*-
import os
import time
import xbmcvfs
import re

from resources.lib.const import SETTINGS, ROUTE, DOWNLOAD_STATUS, DOWNLOAD_SERVICE_EVENT, SERVICE, LANG
from resources.lib.gui import InfoDialog
from resources.lib.kodilogging import logger
from resources.lib.storage.settings import settings
from resources.lib.storage.sqlite import SQLiteStorage, DB
from resources.lib.utils.kodiutils import refresh, current_path, get_string, get_file_path, kodi_json_request, execute_json_rpc, fixed_xbmcvfs_exists
from resources.lib.wrappers.http import Http


def microtime():
    return float(time.time() * 1000)


def get_percentage(pos, total_length):
    return int(100 * pos / total_length)


def dir_check(path):
    dirs_to_check = [] 
    if re.search(r'Season [0-9][0-9]$', path): dirs_to_check.append(os.path.dirname(path))
    dirs_to_check.append(path)
    return dirs_to_check


def download(url, dest, name, dl_id):
    
    for path in dir_check(dest): 
        if not fixed_xbmcvfs_exists(path):
            xbmcvfs.mkdir(path)

    db = SQLiteStorage.Download()
    filename = get_file_path(dest, name)
    logger.debug("Downloading %s to %s " % (str(url), filename))
    if filename[:3] == 'smb' or filename[:3] == 'nfs':
        f = xbmcvfs.File(filename, 'wb')
    else:
        f = open(filename, 'ab+')
    headers = {}
    if os.path.exists(filename):
        pos = os.stat(filename).st_size
    else:
        pos = 0
    if pos:
        headers['Range'] = 'bytes={pos}-'.format(pos=pos)
        logger.debug('Resuming download from position %s' % pos)
    r = Http.get(url, headers=headers, stream=True)
    total_length = int(r.headers.get('content-length'))
    chunk = min(
        32 * 1024 * 1024,
        (1024 * 1024 *
         4) if total_length is None else int(total_length / 100))

    last_notify = None
    last_time = microtime()
    done = get_percentage(pos, total_length)
    db.dl_update(dl_id, done, 0, total_length)
    if ROUTE.DOWNLOAD_QUEUE == current_path():
        refresh()
    dl = 0
    for data in r.iter_content(chunk):
        notify_percent = settings[SETTINGS.DOWNLOADS_NOTIFY]
        status = DB.DOWNLOAD.get_status(dl_id)
        if status != DOWNLOAD_STATUS.DOWNLOADING:
            logger.debug('Downloading cancelled because status changed to %s' % status)
            if status == DOWNLOAD_STATUS.CANCELLED:
                f.close()
                xbmcvfs.delete(filename)
            return
        if total_length is not None:
            dl += len(data)
            t = microtime()
            if t > last_time:
                kbps = int(
                    float(len(data)) / float((t - last_time) / 1000) / 1024)
                done = get_percentage(dl + pos, total_length + pos)
                last_time = t
                if notify_percent != 0 and last_notify != done and (
                        done % notify_percent) == 0:
                    db.dl_update(dl_id, done, kbps, total_length)
                    InfoDialog("%s%% - %d kB/s" % (done, kbps), name).notify()
                    if ROUTE.DOWNLOAD_QUEUE == current_path():
                        refresh()
                    last_notify = done

        else:
            dl += 0
        f.write(data)
    f.close()
    db.done(dl_id)
    InfoDialog(get_string(LANG.SUCCESSFULLY_DOWNLOADED), name).notify()
    if settings[SETTINGS.DOWNLOADS_FOLDER_LIBRARY] and settings[SETTINGS.LIBRARY_AUTO_UPDATE]:
        execute_json_rpc({"jsonrpc": "2.0", "id": 1, "method": "VideoLibrary.Scan"})    # User using downloads to library path and wants to refresh after adding item 
    kodi_json_request(SERVICE.DOWNLOAD_SERVICE, DOWNLOAD_SERVICE_EVENT.ITEM_ADDED, {})  # because we dont know if something isnt next on queue

    if ROUTE.DOWNLOAD_QUEUE == current_path():
        refresh()
