from bisect import bisect
from resources.lib.const import quality_map


def _get_score_audio_language(stream, languages):
    if len(languages) == 0:
        return 0
    for audio in stream.get('audio', []):
        if audio.get('language', '') in languages:
            return 1 / (languages.index(audio.get('language')) + 1)
    return 0


def _get_score_audio_channels(stream, channels=0):
    if channels == 0 or channels is None:
        return 0
    for audio in stream.get('audio', []):
        if audio.get('channels', -1) == channels:
            return 1
    return 0


def _get_score_preffered_codecs(stream, preffered_vcodecs=''):
    if preffered_vcodecs != '':
        preffered_vcodecs = list(map(str.upper, filter(lambda x: x, preffered_vcodecs)))
        for codec in preffered_vcodecs:
            for video in stream.get('video', []):
                if str.upper(codec) == video.get('codec', -1):
                    return 1
    return 0


def _get_score_video_quality(stream, quality):
    if quality == '' or quality is None:
        return 0
    for video in stream.get('video', []):
        if quality == resolution_to_quality(video):
            return 1
    return 0


def _is_bitrate_in_limit(bitrate, limit, tolerance=100):
    """
    :param int bitrate: stream bitrate
    :param int limit: max bitrate in Mbit/s
    :param int tolerance: tolerance in Kbits/s

    returns True if bitrate is in limit + given tolerance or limit is set to 0; False otherwise
    """

    if limit <= 0:
        return True

    limit = float(limit)
    try:
        bitrate = float(bitrate) / 1024 / 1024
    except:
        bitrate = 0
        limit = 0
    return bitrate <= limit + float(tolerance) / 1024


def _get_score_bitrate(stream, max_bitrate):
    size = stream.get('size', 0)
    if size == 0:
        return 0
    for video in filter(lambda v: v.get('duration', 0) != 0, stream.get('video', [])):
        if _is_bitrate_in_limit(size / video.get('duration'), max_bitrate):
            return 1
    return 0


def _score_stream(languages, channels, quality, max_bitrate, preffered_vcodecs):
    def __getscore(stream):
        score = 0
        score += _get_score_audio_language(stream, languages) * 10
        score += _get_score_bitrate(stream, max_bitrate) * 5
        score += _get_score_preffered_codecs(stream, preffered_vcodecs) * 3
        score += _get_score_video_quality(stream, quality) * 2
        score += _get_score_audio_channels(stream, channels)
        return score

    return __getscore


def _avoid_video_codec(codecs):
    codecs = list(map(str.upper, filter(lambda x: x, codecs)))

    def __avoid(stream):
        if len(codecs) == 0:
            return True
        return not all(map(lambda v: v.get('codec', '').upper() in codecs, stream.get('video', [])))

    return __avoid


def select(streams, audio=['cs', 'en'], channels=2, vquality='1080p', codec_blacklist=[], preffered_vcodecs=[], max_bitrate=0):
    streams = filter(_avoid_video_codec(codec_blacklist), streams)
    scores = list(map(_score_stream(audio, channels, vquality, max_bitrate, preffered_vcodecs), streams))

    if len(scores) == 0:
        return None

    return streams[scores.index(max(scores))]


def find_closest_resolution(height):
    keys = sorted(quality_map.keys())
    index = bisect(keys, height)
    return quality_map[keys[index]]


def resolution_to_quality(video):
    height = video.get('height')
    quality = quality_map.get(height) or find_closest_resolution(height)

    return quality


# just for testing, pass in json file with streams list
# PYTHONPATH=. python resources/lib/util/streams.py <path_to_json_file>
if __name__ == '__main__':
    import json
    import sys
    from pprint import pprint

    with open(sys.argv[1]) as f:
        streams = json.load(f)
        pprint(select(streams, codec_blacklist=['h264']))
